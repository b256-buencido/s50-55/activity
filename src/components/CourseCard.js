// import { useState } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

// props or properties acts as a function parameter
export default function CourseCard({courseProp}) {

	// console.log(courseProp);
	// console.log(typeof courseProp);

	// Object Deconstruction
	const { name, description, price, _id} = courseProp;

	// getter -> stores the value. variable
	// setter -> it sets the value to be stored in the getter
	// useState(0) -> initial getter value
	// let maxStudents = 30
	// const [count, setCount] = useState(maxStudents);
	

	// function enroll() {

	// 	if (count == 0) {

	// 		alert("No more seats");

	// 	}
	// 	else {

	// 		setCount(count - 1)
	// 	}

	// 	// setCount(count - 1);

	// 	console.log("Enrollees: " + count)
	// }

	return(
		<Card >
			<Card.Body>
			   	<Card.Title>{name}</Card.Title>
			    <Card.Subtitle>Description:</Card.Subtitle>
			 	<Card.Text>{description}</Card.Text>
			    <Card.Subtitle>Price:</Card.Subtitle>
			    <Card.Text>{price}</Card.Text>
			    {/*<Card.Subtitle>Enrollees:</Card.Subtitle>
			    <Card.Text>{count} Enrollees</Card.Text>*/}
			    <Button variant="primary" as={Link} to={`/courseView/${_id}`} >Details</Button>
			</Card.Body>
		</Card>


	)
}