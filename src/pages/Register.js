import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../userContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();



	// State hooks to store the values of our input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);



	
	useEffect(() => {
		
		if ((firstName !== '' && lastName !== '' && mobileNo !== "" && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {

			setIsActive(true);
		} 
		else {

			setIsActive(false);
		}
	});


	function registerUser(e) {

		e.preventDefault();

		// clear input fields
		setFirstName("");
		setLastName("");
		setMobileNo("");
		setEmail("");
		setPassword1("");
		setPassword2("");

		
	}

	function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
			email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === false) {

				fetch('http://localhost:4000/users/register', 
				{
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password2
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "Welcome to Zuitt!"
					});
				});

				navigate('/login');

			} 
			else {

				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Please provide a different email."
				});
			}
		})
	}

	// onChange - checks if there are any changes inside of the input fields.
	// value={email} - the value stored in the input field will come from the value inside the getter "email"
	// setEmail(e.taget.value) - sets the value of the getter email to the value stored in the value={email} inside the input field

	return (
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
		<Form onSubmit={ registerUser }>
			<Form.Group className="mb-3" controlId="userFirstName">
		    	<Form.Label>First Name</Form.Label>
		    <Form.Control 
		    	type="text" 
		    	placeholder="Enter First Name" 
		    	value={ firstName }
		    	onChange={ e => setFirstName(e.target.value) }
		    	required/>
		  	</Form.Group>

		  	<Form.Group className="mb-3" controlId="userLastName">
		    	<Form.Label>Last Name</Form.Label>
		    <Form.Control 
		    	type="text" 
		    	placeholder="Enter Last Name" 
		    	value={ lastName }
		    	onChange={ e => setLastName(e.target.value) }
		    	required/>
		  	</Form.Group>

		  	<Form.Group className="mb-3" controlId="userMobileNo">
		    	<Form.Label>Mobile Number</Form.Label>
		    <Form.Control 
		    	type="text" 
		    	placeholder="Enter Mobile Number" 
		    	value={ mobileNo }
		    	minLength={11}
		    	maxLength={11}
		    	onChange={ e => setMobileNo(e.target.value) }
		    	required/>
		  	</Form.Group>

		  	<Form.Group className="mb-3" controlId="userEmail">
		    	<Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email" 
		    	value={ email }
		    	onChange={ e => setEmail(e.target.value) }
		    	required/>
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  	</Form.Group>

		  	<Form.Group className="mb-3" controlId="password1">
		    	<Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password"
		    	value={ password1 }
		    	onChange={ e => setPassword1(e.target.value) } 
		    	required/>
		  	</Form.Group>

		  	<Form.Group className="mb-3" controlId="password2">
		    	<Form.Label>Verify Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Verify Password"
		    	value={ password2 }
		    	onChange={ e => setPassword2(e.target.value) }
		    	required/>
		  	</Form.Group>


			{/*Ternary Operator*/}
			{/*
				if (isActive === true) {
					isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
				}
				else {
					<Button variant="danger" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
				}
			*/}

			{
				isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
				:
					<Button variant="danger" type="submit" id="submitBtn" disabled>
						Submit
					</Button>

			}
			
			
    	</Form>
	)
}